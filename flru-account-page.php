<?php
/**
Plugin Name: Free-Lance.ru Widget
Author: Беляков Владислав [ kolerii ]
Author URI: http://kolerii.ru/
Description: Виджет профиля free-lance.ru ( fl.ru )
Version: 1.0.6

  Copyright 2013  Beliakov Vladislav  (email: kolerii@yandex.ru)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/
function freelance_ru_account_page_widget($args)
{
	extract( $args );

	echo $before_widget,$before_title,get_option('freelance_ru_account_page_title'),$after_title;

	$return = json_decode(get_option('freelance_ru_account_page_json'),true);

	$to_replace = array(
							'image' => '{image}',
							'name' => '{username}',
							'rate' => '{rate}',
							'plus' => '{plus}',
							'in_service' => '{in_service}',
							'sbr' => '{sbr}',
							'status' => '{status}',
							'respect' => '{respect}',
							'cost_of_hour' => '{cost_of_hour}',
							'account_url' => '{account_url}',
							'specialty' => '{specialty}',
							'date_reg' => '{date_reg}'
					   );

	$tpl = get_option('freelance_ru_account_page_tpl');
	if(!empty($tpl))
	{
		foreach( $to_replace as $key => $code )
			$tpl = str_replace($code,@$return[$key],$tpl);
	}
	echo stripcslashes($tpl),$after_widget,'<br>';
}

function freelance_geturl( $url )
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_TIMEOUT, 3);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.43 Safari/537.31');
	curl_setopt($ch, CURLOPT_URL, $url );
	$html = iconv('windows-1251','utf8',trim(curl_exec($ch)));
	curl_close($ch);

	return $html;
}

function freelance_ru_parse( $username )
{
	if(empty($username)) return;

	if(!function_exists('file_get_html'))
		include_once( 'simple_html_dom.php' );

	$_tmp = "https://www.fl.ru/users/{$username}/";
	$dom = str_get_html(freelance_geturl($_tmp));

	if(is_object($dom))
	{
		$return = array();

		$ret = $dom->find( 'a[href*="/users/"] img' , 0 );
		if(!empty($ret)) $return['image'] = $ret->src;

		$ret = $dom->find( '.b-layout__txt_padbot_5' , 0 );
		if(!empty($ret)) $return['name'] = trim(str_replace('&#160;','',strip_tags(@reset(explode('<a',$ret->innertext)))));

		$ret = $dom->find( '.ops-plus' , 0);
		if(!empty($ret)) $return['plus'] = trim(str_replace(array('+','&nbsp;'),'',$ret->innertext));

		$ret = $dom->find( 'td.b-layout__td_padright_20 div.b-layout_margbot_3 div span.b-layout__txt_bold' , 0 );
		if(!empty($ret)) $return['rate'] = (float)trim($ret->innertext);

		$ret = $dom->find( 'td div div [href="/promo/bezopasnaya-sdelka/"]' , 0 );
		if(!empty($ret)) $return['sbr'] = trim($ret->prev_sibling()->innertext);

		$ret = $dom->find( '#statusTitle' , 0 );
		if(!empty($ret)) $return['status'] = trim($ret->innertext);

		$ret = $dom->find( '#idPVote' , 0 );
		if(!empty($ret)) $return['respect'] = trim($ret->innertext);

		$ret = $dom->find( 'span.money' , 0 );
		if(!empty($ret)) $return['cost_of_hour'] = trim($ret->innertext);

		$ret = $dom->find( 'div.prtfl-l p' , 0 );
		if(!empty($ret)) $return['specialty'] = trim(strip_tags(str_replace('Специализация:','',$ret->innertext)));

		$return['account_url'] = $_tmp;

		$return['specialty'] = trim(@end(explode('/',$return['specialty'])));

		$dom->clear(); unset( $_tmp , $dom , $ret );
	}

	$dom = str_get_html(freelance_geturl("https://www.fl.ru/users/{$username}/info/"));
	if(is_object($dom))
	{
		$ret = $dom->find('tr th');
		if(!empty($ret))
		{
			$keys = array(
							'На сайте:' => 'in_service',
							'Дата регистрации:' => 'date_reg'
						);
			foreach($ret as $th)
			{

				$key = trim($th->plaintext);
				$value = trim($th->next_sibling()->plaintext);

				if(array_key_exists($key, $keys))
					$return[$keys[$key]] = $value;
			}
		}
		$dom->clear();
	}

	return empty($return) ? false : $return;
}

function register_freelance_ru_account_page()
{
	wp_register_sidebar_widget(
      'freelance_ru_account_page_widget',
      'Free-Lance.ru Widget',
      'freelance_ru_account_page_widget',
      array( 'description' => 'Отображает статус, статистику, аватар и имя.' )
    );
	wp_register_widget_control('freelance_ru_account_page_widget','FL','freelance_ru_account_page_control');
}

function freelance_ru_account_page_control()
{
	$empty_title = !empty($_REQUEST['freelance_ru_account_page_title']);
	$empty_login = !empty($_REQUEST['freelance_ru_account_page_login']);
	$empty_tpl   = !empty($_REQUEST['freelance_ru_account_page_tpl']);

	if($empty_title || $empty_login || $empty_tpl)
	{
		if($empty_title)
			update_option('freelance_ru_account_page_title', $_REQUEST['freelance_ru_account_page_title']);

		if($empty_login)
			update_option('freelance_ru_account_page_login', $_REQUEST['freelance_ru_account_page_login']);

		if($empty_tpl)
			update_option('freelance_ru_account_page_tpl', $_REQUEST['freelance_ru_account_page_tpl']);

		freelance_ru_account_page_cron();
	}

?>	
	<table>
		<tr><td>Заголовок&nbsp;:</td><td><input type="text" name="freelance_ru_account_page_title" value="<?=get_option('freelance_ru_account_page_title')?>"/></td></tr>
		<tr><td>Логин&nbsp;:</td><td><input type="text" name="freelance_ru_account_page_login" value="<?=get_option('freelance_ru_account_page_login')?>"/></td></tr>
	</table>
	Шаблон&nbsp;:<br/>
	<textarea rows="10" style="width:99%;" name="freelance_ru_account_page_tpl">
	<?=stripcslashes(get_option('freelance_ru_account_page_tpl'))?>
	</textarea>
<?php
}

function freelance_ru_account_page_cron()
{
	$return = freelance_ru_parse(get_option('freelance_ru_account_page_login'));
	if(!empty($return['name']))
		update_option('freelance_ru_account_page_json', json_encode($return));
}

function freelance_ru_account_page_activation()
{
	update_option('freelance_ru_account_page_title','Я на Free-Lance.ru');
	update_option('freelance_ru_account_page_tpl',"<center><b>{username}</b></center><br>
		<div style='font-size: 0.78em;'>
		<img src='{image}' style='float:left;'/>
		<span style='margin-left:5px;'><b>Рейтинг:</b> {rate}</span><br>
		<span style='margin-left:5px;'><b>Отзывов:</b> <span style='color:green;'>{plus}</span></span><br>
		<span style='margin-left:5px;'><b>На сайте:</b> {in_service}</span><br>
		<span style='margin-left:5px;'><b>Безопасных сделок:</b> {sbr}</span><br>
		<span style='margin-left:5px;'><b>Статус:</b> {status}</span><br>
		<span style='margin-left:5px;'><b>Репутация:</b> {respect}</span><br>
		<span style='margin-left:5px;'><b>Стоимость часа:</b> <span style='color:green;'>{cost_of_hour}</span></span>
		</div>");

	wp_schedule_event(time(), 'hourly', 'freelance_ru_account_page_cron_action');
}

function freelance_ru_account_page_deactivation()
{
	delete_option('freelance_ru_account_page_title');
	delete_option('freelance_ru_account_page_tpl');
	delete_option('freelance_ru_account_page_login');
	delete_option('freelance_ru_account_page_json');
	wp_clear_scheduled_hook('freelance_ru_account_page_cron_action');
}
add_action('widgets_init','register_freelance_ru_account_page');
add_action('freelance_ru_account_page_cron_action','freelance_ru_account_page_cron');
register_activation_hook(__FILE__,'freelance_ru_account_page_activation');
register_deactivation_hook(__FILE__,'freelance_ru_account_page_deactivation');
?>