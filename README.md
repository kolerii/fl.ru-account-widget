FL.ru account widget
============

This is plugin for demonstration your profile in Free-Lance.ru( FL.ru ).
Requires: WordPress 3.6.1 or later.

"Метки":
* {image}           -  Прямая ссылка на файл аватара.
* {username}     -  Имя, фамилия и псевдоним.
* {rate}	          -  Рейтинг.
* {plus}	          -  Количество положительных отзывов.
* {sbr}	          -  Количество «Безопасных Сделок»
* {status}	          -  Статус, выводится только текст.
* {respect}	  -  Глава «Отношение»
* {cost_of_hour} - Стоимость часа работы.
* {account_url} - Прямая ссылка на аккаунт FL.ru
* {specialty}  - Специальность